class BirdCount
    def self.last_week
    return [0, 2, 5, 3, 7, 8, 4]
    end
   
    def initialize(birds_per_day)
    @yesterday = birds_per_day[-2]
    @total = birds_per_day.sum
    @busy = birds_per_day.count {|birds| birds >= 5}
    @no_birds = birds_per_day.any? {|birds| birds == 0}
    end
   
    def yesterday
    return @yesterday
    end
   
    def total
    return @total
    end
   
    def busy_days
    return @busy
    end
   
    def day_without_birds?
    return @no_birds
    end
 end
    print BirdCount.last_week, "\n"
    puts "-----------------------"
    birds_per_day = [2, 5, 0, 7, 4, 1]
    bird_count = BirdCount.new(birds_per_day)
    print bird_count.yesterday, "\n"
    puts "-----------------------"
    birds_per_day = [2, 5, 0, 7, 4, 1]
    bird_count = BirdCount.new(birds_per_day)
    print bird_count.total, "\n"
    puts "-----------------------"
    birds_per_day = [2, 5, 0, 7, 4, 1]
    bird_count = BirdCount.new(birds_per_day)
    print bird_count.busy_days, "\n"
    puts "-----------------------"
    birds_per_day = [2, 5, 0, 7, 4, 1]
    bird_count = BirdCount.new(birds_per_day)
    print bird_count.day_without_birds?