class LogLineParser
    attr_reader :message, :log_level, :reformat
    
    def initialize(line)
      @line = line
      groups = /\[(?<level>[A-Z]+)\]:\s+(?<msg>.+)/.match(@line)
      @log_level = groups[:level].downcase
      @message = groups[:msg].strip
      @reformat = "#{@message} (#{@log_level})"
    end
  end
  
  
  puts LogLineParser.new('[ERROR]: Invalid operation').message

  puts LogLineParser.new("[WARNING]:  Disk almost full\r\n").message

  puts LogLineParser.new('[ERROR]: Invalid operation').log_level

  puts LogLineParser.new('[INFO]: Operation completed').reformat
 