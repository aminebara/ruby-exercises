class SimpleCalculator
    ALLOWED_OPERATIONS = ['+', '/', '*']
  
    UnsupportedOperation = Class.new(StandardError)
  
    def self.calculate(first_operand, second_operand, operation)
      raise ArgumentError unless (first_operand.is_a?(Numeric) && second_operand.is_a?(Numeric))
      raise UnsupportedOperation.new unless ALLOWED_OPERATIONS.include?(operation)
  
      result = first_operand.public_send(operation, second_operand)
  
      "#{first_operand} #{operation} #{second_operand} = #{result}"
    rescue ZeroDivisionError
      'Division by zero is not allowed.'
    end
  end
  
    
    
  
    puts SimpleCalculator.calculate(16, 51, "+")
  
    puts ('-----------------')
  
    puts SimpleCalculator.calculate(32, 6, "*")
  
    puts ('----------------')
  
    puts SimpleCalculator.calculate(512, 4, "/")
  
    puts ('-----------------')
    
    puts SimpleCalculator.calculate(1, 2, '-')
  
    puts ('-----------------')
  
  
    puts SimpleCalculator.calculate(1, '2', '*')
  
    puts ('---------------')
  
    puts SimpleCalculator.calculate(512, 0, "/")
  
  
  
  
  