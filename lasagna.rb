class Lasagna
    LAYER_PREPARATION_TIME = 2
    EXPECTED_MINUTES_IN_OVEN = 40
    
    def remaining_minutes_in_oven(actual_minutes_in_oven)
        EXPECTED_MINUTES_IN_OVEN - actual_minutes_in_oven
    end
    def preparation_time_in_minutes(layers)
        LAYER_PREPARATION_TIME * layers
    end
    def total_time_in_minutes(number_of_layers:, actual_minutes_in_oven:)
        preparation_time_in_minutes(number_of_layers) + actual_minutes_in_oven
    end
end


puts ('-------------------------- ')

lasagna = Lasagna.new
puts Lasagna::EXPECTED_MINUTES_IN_OVEN

puts ('--------------------------- ')

lasagna = Lasagna.new
puts lasagna.remaining_minutes_in_oven(30)

puts ('--------------------------- ')

lasagna = Lasagna.new
puts lasagna.preparation_time_in_minutes(2)

puts ('--------------------------- ')

lasagna = Lasagna.new
puts lasagna.total_time_in_minutes(number_of_layers: 3, actual_minutes_in_oven: 20)

puts ('---------------------------')
